# Vuepress Plugin Schema
🔌Generate structured data for Your vuepress blog

Add `<script type="application/ld+json">...</script>` to your vuepress site.

## Install

```
npm i vuepress-plugin-schema -D
```

## Usage
Read [How to use vuepress Plugin](https://v1.vuepress.vuejs.org/plugin/using-a-plugin.html) first, modify your `.vuepress/config.js`.
```js
module.exports = {
  plugins: [
    [
      'vuepress-plugin-schema',
      {
        baseURL: 'https://google.com', // base url for your schema, mandatory, default: ''
        title: 'My webpage', // title for your organization schema, mandatory, default: ''
        socials: [  // socials urls for your schema, default: []
            "https://www.youtube.com/channel/CHANNEL_ID",
            "https://www.facebook.com/FACEBOOK_ID/",
        ],
      }
    ]
  ]
}
```

Plugin uses [Frontmatter](https://v1.vuepress.vuejs.org/guide/frontmatter.html) settings with fallback to themeConfig for
these two variables:
```js
const author = $page.frontmatter.author || themeConfig.author; // without it we can't set WebPage and Article schema
const language = $page.frontmatter.lang || themeConfig.language; // without it we can't set Article schema
```

One of these needs always to be set.